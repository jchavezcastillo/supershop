<?php

    require_once 'conexion.php';

    $categoria_id = $_POST['categoria_id'];

    // muestra listado de usuarios

    $consultar = "SELECT * FROM productos WHERE categoria = " . $categoria_id . " AND status = 1";

    $query = mysqli_query(conexion(), $consultar);

    $datos = array();

    foreach ($query as $key => $row) {

    	array_push($datos, 

    		array(

                'id' => htmlentities($row['id']),

    			'nombre' => htmlentities($row['nombre']),

    			'url' => $row['url'],

                'precio' => $row['precio'],
                
    			'precio_app' => $row['precio_app'],

                'existencia' => $row['existencias'],

                'cantidad_minima' => $row['cantidad_minima'],
                
                'unidad_medida' => $row['unidad_medida']

    		)

    	);

	}

    echo json_encode(array('productos' => $datos));

?>