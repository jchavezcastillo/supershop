<?php
    require_once 'conexion.php';
    // muestra listado de usuarios
    $consultar = "SELECT * FROM ofertas WHERE status = 1 ORDER BY fila ASC";
    $query = mysqli_query(conexion(), $consultar);
    $datos = array();
    foreach ($query as $key => $row) {
    	array_push($datos, 
    		array(
    			'nombre' => $row['nombre'],
    			'url' => $row['url'],
    			'fila' => $row['fila'],
    			'descripcion' => $row['descripcion'],
                'precio_anterior' => $row['precio_anterior'],
                'precio_actual' => $row['precio_actual'],
                'existencia' => $row['existencia']
    		)
    	);
	}
    echo json_encode(array('ofertas' => $datos));
?>