<?php

    require_once 'conexion.php';
    $conexion=conexion();

    $nombre_producto = $_POST['producto'];

    if (empty($nombre_producto)) {
        echo "No hay resultados";
    }else {

        // muestra listado de productos


        $consultar = "SELECT * FROM productos WHERE nombre LIKE '%".$nombre_producto."%' AND status = 1 LIMIT 5";
        
        $query = mysqli_query($conexion, $consultar);

        $datos = array();

        foreach ($query as $key => $row) {

            array_push($datos, 

                array(

                    'id' => htmlentities($row['id']),

                    'nombre' => htmlentities($row['nombre']),

                    'url' => $row['url'],

                    'precio' => $row['precio'],
                    
                    'precio_app' => $row['precio_app'],

                    'existencia' => $row['existencias'],

                    'cantidad_minima' => $row['cantidad_minima'],
                    
                    'unidad_medida' => $row['unidad_medida']

                )

            );

        }

        echo json_encode(array('productos' => $datos));
    }

?>