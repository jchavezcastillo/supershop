<!DOCTYPE html>
<html>
<head>
	<title>Login</title>

	<?php require_once "scripts.php"; ?>
</head>
<body style="background: dimgray;">
<br><br><br>
<div class="container">
	<div class="row">
		<div class="col-sm-4">
        </div>
		<!-- Clientes -->
		<div class="col-sm-4">
			<div class="panel panel-primary">
				<div class="panel panel-body">
					<p></p>
					<label>Usuario</label>
					<input type="text" id="usuario" class="form-control input-sm" name="">
					<label>Password</label>
					<input type="password" id="password" class="form-control input-sm" name="">
					<p></p>
					<span class="btn btn-primary" id="login">Iniciar Sesión</span>
					<a href="registro.php" class="btn btn-success">Registro</a>
					<a href="entregas.php" class="btn btn-danger">Entregas</a>
				</div>
			</div>
		</div>
		<div class="col-sm-4">
        </div>
	</div>
</div>
</body>
</html>

<style>
    #map {
    width: 100%;
    height: 500px;
    background-color: grey;
    }
</style>

<script type="text/javascript">
	// Login
	$(document).ready(function(){
		$('#login').click(function(){
			if($('#usuario').val()==""){
				alertify.alert("Ingrese un usuario");
				return false;
			}else if($('#password').val()==""){
				alertify.alert("Ingrese su contraseña");
				return false;
			}

			let usuario = $('#usuario').val(),
			contraseña = $('#password').val(),

			datos={
				"usuario": usuario,
				"password": contraseña 
				};

					$.ajax({
						type:"POST",
						url:"CRUD/login.php",
						data:datos,
						dataType: 'JSON',
						success:function(r){
							if(r.success==1){
								window.location="inicio.php";
							}else{
								alertify.error("Falló al entrar");
							}
						}
					});
		});	
	});
</script>