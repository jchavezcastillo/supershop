<?php
    require_once 'scripts.php';
    require_once 'CRUD/conexion.php';

    // muestra listado de pedidos
    //$consultar = "SELECT * FROM `pedidos`";
    $consultar = "SELECT p.id, p.forma_pago, p.fecha, p.usuario, p.total, u.colonia, u.calle_numero, u.telefono, u.email FROM pedidos p JOIN usuarios u on(p.usuario=u.usuario)";
    $query = mysqli_query(conexion(), $consultar);

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Pedidos</title>
</head>
<body>
    <div class="col-md-12" style="text-align: right; padding: 30px 60px 0 0;">
        <a href="ver-productos.php" class="btn btn-lg btn-primary">Actualizar Productos</a>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-md-1"></div>
            <div class="col-md-10">
                <h2 style="text-align:center">Listado de Pedidos</h2>
                <table class="table table-hover pedidos" style="margin-top:5%;">
                    <thead>
                        <th>Folio</th>
                        <th>Fecha</th>
                        <th>Usuario</th>
                        <th>Total</th>
                        <th>Forma de pago</th>
                        <th>Enviar a</th>
                        <th>Estatus</th>
                        <th></th>
                    </thead>
                    <tbody>
                    <?php
                        foreach ($query as $key => $value) {
                            
                    ?>
                        <tr id="tableTR" data-idpedido="<?php echo $value['id']?>">
                            <td><?php echo $value['id']?></td>
                            <td>
                                <?php echo $value['fecha'] . "<br>"?> <!-- date("d-m-Y", strtotime( )) -->
                            </td>
                            <td><?php echo $value['usuario']?></td>
                            <td><?php echo $value['total']?></td>
                            <td><?php 
                                switch ($value['forma_pago']) {
                                    case '1':
                                        echo "Pago contra entrega";
                                        break;
                                    case '2':
                                        echo "Pago con tarjeta débito o crédito";
                                        break;
                                    case '3':
                                        echo "Pago en tienda OXXO";
                                        break;
                                    case '4':
                                        echo "Transferencia interbancaria BBVA";
                                        break;
                                    }
                             ?></td>
                            <td><?php echo $value['colonia'] . ", " . $value['calle_numero'] . ", " . $value['telefono'] . ", " . $value['correo'] ?></td>
                            <td>
                                <?php
                                if ($value['status'] == 1) {
                                ?>
                                    <select id="estadoPedido" class="form-control select-estatusPedido" style="width: 80%;">
                                        <option value="0">seleccione estatus</option>
                                        <option selected value="1">Nuevo Pedido</option>
                                        <option value="2">Enviando Pedido</option>
                                        <option value="3">Recibido y Pagado</option>
                                        <option value="4">Pausado</option>
                                    </select>
                                <?php
                                }elseif ($value['status'] == 2) {
                                ?>
                                    <select id="estadoPedido" class="form-control select-estatusPedido" style="width: 80%;">
                                        <option value="0">seleccione estatus</option>
                                        <option value="1">Nuevo Pedido</option>
                                        <option selected value="2">Enviando Pedido</option>
                                        <option value="3">Recibido y Pagado</option>
                                        <option value="4">Pausado</option>
                                    </select>
                                <?php
                                }elseif ($value['status'] == 3) {
                                ?>
                                    <select id="estadoPedido" class="form-control select-estatusPedido" style="width: 80%;">
                                        <option value="0">seleccione estatus</option>
                                        <option value="1">Nuevo Pedido</option>
                                        <option value="2">Enviando Pedido</option>
                                        <option selected value="3">Recibido y Pagado</option>
                                        <option value="4">Pausado</option>
                                    </select>
                                <?php
                                }else {
                                ?>
                                    <select id="estadoPedido" class="form-control select-estatusPedido" style="width: 80%;">
                                        <option value="0">seleccione estatus</option>
                                        <option value="1">Nuevo Pedido</option>
                                        <option value="2">Enviando Pedido</option>
                                        <option value="3">Recibido y Pagado</option>
                                        <option selected value="4">Pausado</option>
                                    </select>
                                <?php
                                }
                                ?>
                            </td>
                            <td>
                                  <a type="button" id="detalleProducto" class="btn btn-sm btn-success" href="detallePedidos.php?id=<?php echo $value['id']?>" data-detalleid="<?php echo $value['id'] ?>" >Detalle</a>
                                  <a type="button" id="" class="btn btn-sm btn-warning" href="ticket-detalle.php?folio=<?php echo $value['id'] ?>" target="_blank">Imprimir Ticket</a>
                                  <!-- <input type="text" class="form-control input-sm" id="inputEstatus" name="" value=""> -->
                            </td>
                        </tr>
                    </tbody>
                    <?php
                        }
                    ?>
                </table>
            </div>
            <div class="col-md-1"></div>
        </div>
    </div>
</body>
</html>
<script>
    $('.select-estatusPedido').on('change', function(){
        let id = $(this).parents('tr#tableTR').data("idpedido"),
            statuspedido = $(this).find('option:selected').val(),

        data={
            "idPedido": id,
            "status": statuspedido
        };
	
        $.ajax({
            type:"POST",
            url:"CRUD/statusPedido.php",
            data:data,
            dataType: 'JSON',
            success:function(r){
                if(r==1){
                    alertify.success('Estatus de ID '+ id + ' actualizado');
                }else{
                    alertify.error("Falló al actualizar estatus");
                }
            }
    });
})
</script>




