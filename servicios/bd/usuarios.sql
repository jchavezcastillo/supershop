
create database superShop;

use superShop;

create table usuarios(
	id_cliente int auto_increment,
	nombre varchar(500),
	telefono bigint(20),
	email varchar(300),
	calle_numero varchar (500),
	colonia varchar (500),
	usuario varchar(300),
	password varchar(500),
	primary key(id_cliente)
);