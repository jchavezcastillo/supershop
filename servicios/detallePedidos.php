<?php
    require_once 'scripts.php';
    require_once 'CRUD/conexion.php';

    // muestra detalle del pedidos
    $id_pedido  = $_GET['id'];
    $consultarDetalle = "SELECT * FROM detalle_pedidos WHERE pedido_id = " . $id_pedido;
    $queryDetalle = mysqli_query(conexion(), $consultarDetalle);

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Detalle Pedidos</title>
</head>
<body>
    <div class="container">
        <div class="row">
            <div class="col-md-12">
            <h2 style="text-align:center">Detalle de Productos</h2>
            <table class="table table-hover" style="margin-top:5%">
                <thead>
                    <th>Pedido No.</th>
                    <th style="text-align:center">Imagen</th>
                    <th>Nombre</th>
                    <th>Precio Normal</th>
                    <th>Precio C/Descuento</th>
                    <th>Cantidad</th>
                    <th>Total</th>
                    <th>Estatus</th>
                </thead>
                <tbody>
                    <?php
                        while ($mostrar=mysqli_fetch_array($queryDetalle))
                        {
                    ?>
                        <tr>
                            <td><?php echo $mostrar['id'] ?></td>
                            <td>
                                <center>
                                    <?php echo '<img src="../'.$mostrar['imagen'].'" alt="" width="50%">'?>
                                </center>
                            </td>
                            <td><?php echo $mostrar['nombre']?></td>
                            <td><?php echo $mostrar['precio_normal']?></td>
                            <td><?php echo $mostrar['precio_descuento']?></td>
                            <td><?php echo $mostrar['cantidad']?></td>
                            <td><?php echo $mostrar['total']?></td>
                            <td><?php echo $mostrar['status']?></td>
                        </tr>
                </tbody>
                    <?php
                        }
                    ?>
            </table>
            </div>
        </div>
    </div>
    
</body>
</html>