<!DOCTYPE html>
<html>
<head>
	<title>Entregas</title>
	<?php require_once "scripts.php"; ?>
</head>
<body style="background-color: gray">
<br><br><br>
<div class="container">
	<div class="row">
		<div class="col-sm-4"></div>
		<div class="col-sm-4">
			<div class="panel panel-success">
				<div class="panel panel-heading" style="text-align:center">Enviar Pedido</div>
				<div class="panel panel-body">
					<form id="frmEntrega">
						<label>ID del Pedido</label>
						<input type="text" class="form-control input-sm" id="idPedido" name="">
						<p></p>
						<span style="margin-left:40%" class="btn btn-success" id="enviarPedido">Enviar</span>
					</form>
					<!-- <div style="text-align: right;">
						<a href="index.php" class="btn btn-default">Login</a>
					</div> -->
				</div>
			</div>
		</div>
		<div class="col-sm-4"></div>
	</div>
</div>
</body>
</html>

<script type="text/javascript">
	$(document).ready(function(){
		$('#enviarPedido').click(function(){

			if($('#idPedido').val()==""){
				alertify.alert("Ingrese ID del Pedido");
				return false;
			}

                enviarPedido = "&pedidoID=" + $('#idPedido').val();
                $.ajax({
                    type:"POST",
                    url:"CRUD/entrega.php",
                    data:enviarPedido,
                    success:function(r){
                        if(r==1){
                            $('#frmEntrega')[0].reset();
                            alertify.success("Pedido Enviado");
                            // setTimeout(() => {
                            // window.history.back(-1);
                            // }, 500);
                        }else{
                            alertify.error("Falló al enviar pedido");
                        }
                    }
                });
		});
	});
</script>

