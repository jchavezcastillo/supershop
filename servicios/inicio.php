<?php 
	session_start();

	if(isset($_SESSION['user'])){
?>

<?php
	require_once 'CRUD/consultas.php';
?>

<!DOCTYPE html>
<html>
<head>
	<title>Inicio</title>
	<?php require_once "scripts.php"; ?>
</head>
<body>
<br><br><br>
	<div class="container">
		<div class="row">
			<div class="col-sm-12">
				<div class="jumbotron">
					<div style="text-align: right;">
						<a href="CRUD/salir.php" class="btn btn-danger">Cerar Sesión</a>
					</div>
					<h2 style="text-align:center">Datos de entrega</h2>
				</div>
			</div>
			<a href="registro.php" class="btn btn-success">Nuevo Cliente</a>
			<a href="pedidos.php" class="btn btn-warning">Pedidos</a>

			<br><br>
			<div>
				<table class="table table-hover">
					<thead>
						<th>Nombre</th>
						<th>Teléfono</th>
						<th>Email</th>
						<th>Calle y Número Ext.</th>
						<th>Colonia</th>
						<th>Acciones</th>
					</thead>
					<tbody>
						<?php
							foreach ($query as $key => $row) { ?>
								<tr <?php echo $row['id_cliente']?>>
									<td><?php echo $row['nombre']?></td>
									<td><?php echo $row['telefono']?></td>
									<td><?php echo $row['email']?></td>
									<td><?php echo $row['calle_numero']?></td>
									<td><?php echo $row['colonia']?></td>
									<td>
										<!-- <button id="editar" class="btn btn-sm btn-primary" > -->
											<a class="btn btn-sm btn-primary" href="actualizar.php?id_cliente=<?php echo $row['id_cliente']?>">Editar</a>
										<!-- </button> -->
										<button id="eliminar" onclick="borrar()" class="btn btn-sm btn-primary">Eliminar</button>
									</td>
								</tr>
					</tbody>
					<?php
						}
					?>
				</table>
			</div>
		</div>
	</div>
</body>
</html>

<?php
} else {
	header("location:index.php");
	}
?>

<script>

function actualizar() {
	alert(<?php echo $row['id_cliente']?>);
}

function borrar() {
	alert("eliminar");
}

</script>
