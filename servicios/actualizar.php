<?php
	require_once "CRUD/conexion.php";
	$id = $_GET['id_cliente'];
    $consulta = "SELECT * FROM  usuarios where id_cliente = '$id'";
    $resultado = mysqli_query(conexion(), $consulta);

?>

<!DOCTYPE html>
<html>
<head>
	<title>Registro</title>
	<?php require_once "scripts.php"; ?>
</head>
<body style="background-color: gray">
<br><br><br>
<div class="container">
	<div class="row">
		<div class="col-sm-4"></div>
		<div class="col-sm-4">
			<div class="panel panel-success">
				<div class="panel panel-heading" style="text-align:center">Datos Personales</div>
				<div class="panel panel-body">
					<form id="frmActualizar">

					<?php
						while ($mostrar=mysqli_fetch_array($resultado)) {
					
					?>
						<input type="hidden" class="form-control input-sm" id="updateId" name="" value="<?php echo $mostrar['id_cliente']?>">
						<label>Nombre</label>
						<input type="text" class="form-control input-sm" id="updateNombre" name="" value="<?php echo $mostrar['nombre']?>">
						<label>Telefono</label>
						<input type="text" class="form-control input-sm" id="updateTelefono" name="" value="<?php echo $mostrar['telefono']?>">
						<label>Email</label>
						<input type="text" class="form-control input-sm" id="updateEmail" name="" value="<?php echo $mostrar['email']?>">
						<label>Calle y número</label>
						<input type="text" class="form-control input-sm" id="updateCalleNumero" name="" value="<?php echo $mostrar['calle_numero']?>">
						<label>Colonia</label>
						<input type="text" class="form-control input-sm" id="updateColonia" name="" value="<?php echo $mostrar['colonia']?>">
						<p></p>
						<span class="btn btn-primary" id="actualizarUsuario">Actualizar</span>
						<!-- <input type="submit" class="btn btn-primary" name="" id="" value="Actualizar"> -->
					<?php
						}
					?>
					</form>
					<!-- <div style="text-align: right;">
						<a href="index.php" class="btn btn-default">Login</a>
					</div> -->
					<div style="text-align: right;">
						<a href="inicio.php" class="btn btn-default">Regresar</a>
					</div>
				</div>
			</div>
		</div>
		<div class="col-sm-4"></div>
	</div>
</div>
</body>
</html>

<script type="text/javascript">
	$(document).ready(function(){
		$('#actualizarUsuario').click(function(){

			if($('#updateNombre').val()==""){
				alertify.alert("Ingresa el nombre");
				return false;
			}else if($('#updateTelefono').val()==""){
				alertify.alert("Ingresa el telefono");
				return false;
			}else if($('#updateEmail').val()==""){
				alertify.alert("Ingresa el email");
				return false;
			}else if($('#updateCalleNumero').val()==""){
				alertify.alert("Ingresa la calle y numero ext.");
				return false;
			}else if($('#updateColonia').val()==""){
				alertify.alert("Ingresa la colonia");
				return false;
			}

			cadena= "id=" + $('#updateId').val() +
					"&nombre=" + $('#updateNombre').val() +
					"&telefono=" + $('#updateTelefono').val() +
					"&email=" + $('#updateEmail').val() +
					"&calle_numero=" + $('#updateCalleNumero').val() + 
					"&colonia=" + $('#updateColonia').val()
					;

					$.ajax({
						type:"POST",
						url:"CRUD/actualizar.php",
						data:cadena,
						success:function(r){
							if(r==1){
								alertify.success("Usuario actualizado");
								setTimeout(() => {
									location.href ='inicio.php';
								}, 500);
							}else{
								alertify.error("Fallo al actualizar");
							}
						}
					});
		});
	});
</script>

