<?php

    require_once 'scripts.php';

    require_once 'CRUD/conexion.php';

?>

<!DOCTYPE html>

<html lang="en">

<head>

    <meta charset="UTF-8">

    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <title>Productos</title>

    <meta http-equiv="Content-type" content="text/html; charset=utf-8" />

    <!-- Latest compiled and minified CSS -->

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

    <!-- Optional theme -->

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">



    <script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>

    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>



    <!-- Latest compiled and minified JavaScript -->

    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>

</head>
<style>
    .tab {
        overflow: hidden;
        border: 1px solid #ccc;
        background-color: #f1f1f1;
        padding: 0 235px;
    }

    /* Style the buttons that are used to open the tab content */
    .tab button {
        background-color: inherit;
        float: left;
        border: none;
        outline: none;
        cursor: pointer;
        padding: 14px 16px;
        transition: 0.3s;
    }

    /* Change background color of buttons on hover */
    .tab button:hover {
        background-color: #ddd;
    }

    /* Create an active/current tablink class */
    .tab button.active {
        background-color: #ccc;
    }

    /* Style the tab content */
    .tabcontent {
        display: none;
        padding: 6px 12px;
        border: 1px solid #ccc;
        border-top: none;
    }
</style>

<body>
    
    <div class="col-md-12" style="text-align: right; padding: 30px 60px 30px 0;">
        <a href="pedidos.php" class="btn btn-lg btn-primary">Ver Pedidos</a>
        <h2 style="text-align:center">Actualizar Productos</h2>
    </div>

    <!-- Tab links -->

    <div class="tab">
        <button class="tablinks" onclick="openMenu(event, 'frutas-verduras')">
            <h4>
                Listado de frutas y verduras
            </h4>    
        </button>
        <button class="tablinks" onclick="openMenu(event, 'curacion')">
            <h4>
                Listado de materiales de curación
            </h4>            
        </button>
        <button class="tablinks" onclick="openMenu(event, 'abarrotes')">
            <h4>
                Listado de abarrotes
            </h4>            
        </button>
        <button class="tablinks" onclick="openMenu(event, 'carnes')">
            <h4>
                Listado de carnes
            </h4>            
        </button>
        <button class="tablinks" onclick="openMenu(event, 'pescaderia')">
            <h4>
                Listado pescaderia
            </h4>    
        </button>
        <button class="tablinks" onclick="openMenu(event, 'cosas-bebe')">
            <h4>
                Listado cosas de bebé
            </h4>            
        </button>
    </div>


    <!-- Tab content -->

    <div class="tabcontent" id="frutas-verduras" style="">
        <?php 

            /*FRUTAS Y VERDURAS*/

            $consultarP1 = "SELECT * FROM productos where categoria = 8";

            $queryP1 = mysqli_query(conexion(), $consultarP1);

        ?>

        <!-- <h2 style="text-align:center">Listado de frutas y verduras</h2> -->

        <table class="table table-hover pedidos" style="margin-top:5%;">

            <thead>

                <th>Imágen</th>

                <th>Nombre</th>

                <th>Precio normal</th>

                <th>Precio en la app con 30%</th>

                <th>Existencias</th>

                <th>Cantidad minima</th>

                <th>Unidad de medida</th>

                <th></th>

            </thead>

            <tbody>

            <?php

                foreach ($queryP1 as $key => $value) {

                    

            ?>

                <tr id="tableTR" data-idpedido="<?php echo $value['id']?>">

                    <td><img style="max-width: 200px;" src="../<?php echo $value['url']?>"></td>

                    <td>

                        <input type="" name="" id="nombre" value="<?php echo $value['nombre']?>">

                    </td>

                    <td>

                        <input type="" name="" id="precio" value="<?php echo $value['precio']?>">

                    </td>

                    <td>

                        <input type="" name="" id="precio_app" value="<?php echo $value['precio_app']?>" disabled>

                    </td>

                    <td>

                        <input type="" name="" id="existencias" value="<?php echo $value['existencias']?>">

                    </td>

                    <td>

                        <input type="" name="" id="cantidad_minima" value="<?php echo $value['cantidad_minima']?>">

                    </td>

                    <td>

                        <input type="" name="" id="unidad_medida" disabled value="<?php echo $value['unidad_medida']?>">

                    </td>

                    <td>

                        <button class="btn btn-success" id="guardar" data-id="<?php echo $value['id']?>">Guardar</button>

                    </td>

                </tr>

            

            <?php

                }

            ?>

            </tbody>

        </table>

    </div>


    <div class="tabcontent" id="curacion" style="">
        <?php 

            /*MATERIALES DE CURACIÓN*/

            $consultarP3 = "SELECT * FROM productos where categoria = 1";

            $queryP3 = mysqli_query(conexion(), $consultarP3);

        ?>

        <!-- <h2 style="text-align:center">Listado de materiales de curación</h2> -->

        <table class="table table-hover pedidos" style="margin-top:5%;">

            <thead>

                <th>Imágen</th>

                <th>Nombre</th>

                <th>Precio normal</th>

                <th>Precio en la app con 30%</th>

                <th>Existencias</th>

                <th>Cantidad minima</th>

                <th>Unidad de medida</th>

                <th></th>

            </thead>

            <tbody>

            <?php

                foreach ($queryP3 as $key => $value) {



            ?>

                <tr id="tableTR" data-idpedido="<?php echo $value['id']?>">

                    <td><img style="max-width: 200px;" src="../<?php echo $value['url']?>"></td>

                    <td>

                        <input type="" name="" id="nombre" value="<?php echo utf8_encode($value['nombre'])?>">

                    </td>

                    <td>

                        <input type="" name="" id="precio" value="<?php echo $value['precio']?>">

                    </td>

                    <td>

                        <input type="" name="" id="precio_app" value="<?php echo $value['precio_app']?>" disabled>

                    </td>

                    <td>

                        <input type="" name="" id="existencias" value="<?php echo $value['existencias']?>">

                    </td>

                    <td>

                        <input type="" name="" id="cantidad_minima" value="<?php echo $value['cantidad_minima']?>">

                    </td>

                    <td>

                        <input type="" name="" id="unidad_medida" disabled value="<?php echo $value['unidad_medida']?>">

                    </td>

                    <td>

                        <button class="btn btn-success" id="guardar" data-id="<?php echo $value['id']?>">Guardar</button>

                    </td>

                </tr>

            <?php

                }

            ?>

            </tbody>

        </table>

    </div>


    <div class="tabcontent" id="abarrotes" style="">
        <?php 

            /* ABARROTES*/

            $consultarP3 = "SELECT * FROM productos where categoria = 9";

            $queryP3 = mysqli_query(conexion(), $consultarP3);

        ?>

        <!-- <h2 style="text-align:center">Listado de abarrotes</h2> -->

        <table class="table table-hover pedidos" style="margin-top:5%;">

            <thead>

                <th>Imágen</th>

                <th>Nombre</th>

                <th>Precio normal</th>

                <th>Precio en la app con 30%</th>

                <th>Existencias</th>

                <th>Cantidad minima</th>

                <th>Unidad de medida</th>

                <th></th>

            </thead>

            <tbody>

            <?php

                foreach ($queryP3 as $key => $value) {



            ?>

                <tr id="tableTR" data-idpedido="<?php echo $value['id']?>">

                    <td><img style="max-width: 200px;" src="../<?php echo $value['url']?>"></td>

                    <td>

                        <input type="" name="" id="nombre" value="<?php echo utf8_encode($value['nombre'])?>">

                    </td>

                    <td>

                        <input type="" name="" id="precio" value="<?php echo $value['precio']?>">

                    </td>

                    <td>

                        <input type="" name="" id="precio_app" value="<?php echo $value['precio_app']?>" disabled>

                    </td>

                    <td>

                        <input type="" name="" id="existencias" value="<?php echo $value['existencias']?>">

                    </td>

                    <td>

                        <input type="" name="" id="cantidad_minima" value="<?php echo $value['cantidad_minima']?>">

                    </td>

                    <td>

                        <input type="" name="" id="unidad_medida" disabled value="<?php echo $value['unidad_medida']?>">

                    </td>

                    <td>

                        <button class="btn btn-success" id="guardar" data-id="<?php echo $value['id']?>">Guardar</button>

                    </td>

                </tr>

            <?php

                }

            ?>

            </tbody>

        </table>

    </div>


    <div class="tabcontent" id="carnes" style="">
        <?php 

            /* CARNES*/

            $consultarP3 = "SELECT * FROM productos where categoria = 11";

            $queryP3 = mysqli_query(conexion(), $consultarP3);

        ?>

        <!-- <h2 style="text-align:center">Listado de carnes</h2> -->

        <table class="table table-hover pedidos" style="margin-top:5%;">

            <thead>

                <th>Imágen</th>

                <th>Nombre</th>

                <th>Precio normal</th>

                <th>Precio en la app con 30%</th>

                <th>Existencias</th>

                <th>Cantidad minima</th>

                <th>Unidad de medida</th>

                <th></th>

            </thead>

            <tbody>

            <?php

                foreach ($queryP3 as $key => $value) {



            ?>

                <tr id="tableTR" data-idpedido="<?php echo $value['id']?>">

                    <td><img style="max-width: 200px;" src="../<?php echo $value['url']?>"></td>

                    <td>

                        <input type="" name="" id="nombre" value="<?php echo utf8_encode($value['nombre'])?>">

                    </td>

                    <td>

                        <input type="" name="" id="precio" value="<?php echo $value['precio']?>">

                    </td>

                    <td>

                        <input type="" name="" id="precio_app" value="<?php echo $value['precio_app']?>" disabled>

                    </td>

                    <td>

                        <input type="" name="" id="existencias" value="<?php echo $value['existencias']?>">

                    </td>

                    <td>

                        <input type="" name="" id="cantidad_minima" value="<?php echo $value['cantidad_minima']?>">

                    </td>

                    <td>

                        <input type="" name="" id="unidad_medida" disabled value="<?php echo $value['unidad_medida']?>">

                    </td>

                    <td>

                        <button class="btn btn-success" id="guardar" data-id="<?php echo $value['id']?>">Guardar</button>

                    </td>

                </tr>

            <?php

                }

            ?>

            </tbody>

        </table>

    </div>
    

    <div class="tabcontent" id="pescaderia" style="">
        <?php 

            /*PESCADERIA*/

            $consultarP2 = "SELECT * FROM productos where categoria = 10";

            $queryP2 = mysqli_query(conexion(), $consultarP2);

        ?>

        <!-- <h2 style="text-align:center">Listado pescaderia</h2> -->

        <table class="table table-hover pedidos" style="margin-top:5%;">

            <thead>

                <th>Imágen</th>

                <th>Nombre</th>

                <th>Precio normal</th>

                <th>Precio en la app con 30%</th>

                <th>Existencias</th>

                <th>Cantidad minima</th>

                <th>Unidad de medida</th>

                <th></th>

            </thead>

            <tbody>

            <?php

                foreach ($queryP2 as $key => $value) {

                    

            ?>

                <tr id="tableTR" data-idpedido="<?php echo $value['id']?>">

                    <td><img style="max-width: 200px;" src="../<?php echo $value['url']?>"></td>

                    <td>

                        <input type="" name="" id="nombre" value="<?php echo $value['nombre']?>">

                    </td>

                    <td>

                        <input type="" name="" id="precio" value="<?php echo $value['precio']?>">

                    </td>

                    <td>

                        <input type="" name="" id="precio_app" value="<?php echo $value['precio_app']?>" disabled>

                    </td>

                    <td>

                        <input type="" name="" id="existencias" value="<?php echo $value['existencias']?>">

                    </td>

                    <td>

                        <input type="" name="" id="cantidad_minima" value="<?php echo $value['cantidad_minima']?>">

                    </td>

                    <td>

                        <input type="" name="" id="unidad_medida" disabled value="<?php echo $value['unidad_medida']?>">

                    </td>

                    <td>

                        <button class="btn btn-success" id="guardar" data-id="<?php echo $value['id']?>">Guardar</button>

                    </td>

                </tr>

            

            <?php

                }

            ?>

            </tbody>

        </table>

    </div>


    <div class="tabcontent" id="cosas-bebe" style="">
        <?php 

            /*COSAS DE BEBÉ*/

            $consultarP4 = "SELECT * FROM productos where categoria = 6";

            $queryP4 = mysqli_query(conexion(), $consultarP4);

        ?>

        <!-- <h2 style="text-align:center">Listado cosas de bebé</h2> -->

        <table class="table table-hover pedidos" style="margin-top:5%;">

            <thead>

                <th>Imágen</th>

                <th>Nombre</th>

                <th>Precio normal</th>

                <th>Precio en la app con 30%</th>

                <th>Existencias</th>

                <th>Cantidad minima</th>

                <th>Unidad de medida</th>

                <th></th>

            </thead>

            <tbody>

            <?php

                foreach ($queryP4 as $key => $value) {



            ?>

                <tr id="tableTR" data-idpedido="<?php echo $value['id']?>">

                    <td><img style="max-width: 200px;" src="../<?php echo $value['url']?>"></td>

                    <td>

                        <input type="" name="" id="nombre" value="<?php echo utf8_encode($value['nombre'])?>">

                    </td>

                    <td>

                        <input type="" name="" id="precio" value="<?php echo $value['precio']?>">

                    </td>

                    <td>

                        <input type="" name="" id="precio_app" value="<?php echo $value['precio_app']?>" disabled>

                    </td>

                    <td>

                        <input type="" name="" id="existencias" value="<?php echo $value['existencias']?>">

                    </td>

                    <td>

                        <input type="" name="" id="cantidad_minima" value="<?php echo $value['cantidad_minima']?>">

                    </td>

                    <td>

                        <input type="" name="" id="unidad_medida" disabled value="<?php echo $value['unidad_medida']?>">

                    </td>

                    <td>

                        <button class="btn btn-success" id="guardar" data-id="<?php echo $value['id']?>">Guardar</button>

                    </td>

                </tr>

            <?php

                }

            ?>

            </tbody>

        </table>

    </div>



    <!-- Modal FIN DE LA COMPRA-->

    <div class="modal fade" id="loadingModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">

        <div class="modal-dialog" role="document">

            <div class="modal-content">

                <div class="modal-body">

                    <div class="row">

                        <div class="col-sm-12" style="text-align: center;">

                            <img src="img/loading.gif" alt="" style="max-width: 100px;">

                        </div>

                    </div>

                </div>

            </div>

        </div>

    </div>



</body>

</html>

<script>

   $(document).on("click", "#guardar", function(){



        var data = {

            id:$(this).data('id'),

            nombre: $(this).parents('#tableTR').find('#nombre').val(),

            precio_normal:$(this).parents('#tableTR').find('#precio').val(),

            precio_app:$(this).parents('#tableTR').find('#precio_app').val(),

            existencia:$(this).parents('#tableTR').find('#existencias').val(),

            cantidad_minima:$(this).parents('#tableTR').find('#cantidad_minima').val(),

            unidad_medida:$(this).parents('#tableTR').find('#unidad_medida').val()

        }



        $("#loadingModal").modal({ backdrop: 'static', keyboard: false, show: true });



        $.ajax({

            type:"POST",

            url:"CRUD/guardarProducto.php",

            data: data,

            dataType:"json",

            success:function(response){

                if(!response.success){

                    alert("No se pudo actualizar el producto.");

                }

            }

        });

        $("#loadingModal").modal('hide');

   });

    function openMenu(evt, cityName) {
        var i, tabcontent, tablinks;
        tabcontent = document.getElementsByClassName("tabcontent");
        for (i = 0; i < tabcontent.length; i++) {
            tabcontent[i].style.display = "none";
        }
        tablinks = document.getElementsByClassName("tablinks");
        for (i = 0; i < tablinks.length; i++) {
            tablinks[i].className = tablinks[i].className.replace(" active", "");
        }
        document.getElementById(cityName).style.display = "block";
        evt.currentTarget.className += " active";
    }

    $(document).on('change','#precio',function(){
        var cantInput = $(this).val();
        var comision = Number(cantInput * .30);
        var precioApp = Number(cantInput) + Number(comision);

        $(this).parents('#tableTR').find('#precio_app').val(precioApp)
        
        alert('La comisión es: ' + comision)
        alert('Precio-App: ' + precioApp)

    });
</script>









