<!DOCTYPE html>
<html>
<head>
	<title>Registro</title>
	<?php require_once "scripts.php"; ?>
	<meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Registro</title>
</head>
<body style="background-color: gray">
<br><br><br>
<div class="container">
	<div class="row">
		<div class="col-sm-4"></div>
		<div class="col-sm-4">
			<div class="panel panel-success">
				<div class="panel panel-heading" style="text-align:center">Datos Personales</div>
				<div class="panel panel-body">
					<form id="frmRegistro">
						<label>Nombre completo</label>
						<input type="text" class="form-control input-sm" id="nombre" name="">
						<label>Teléfono</label>
						<input type="text" class="form-control input-sm" id="telefono" name="">
						<label>Correo</label>
						<input type="text" class="form-control input-sm" id="email" name="">
						<label>Calle y número</label>
						<input type="text" class="form-control input-sm" id="calle_numero" name="">
						<label>Colonia</label>
						<input type="text" class="form-control input-sm" id="colonia" name="">
						<!--<label>Usuario</label>
						<input type="text" class="form-control input-sm" id="usuario" name="">-->
						<label>Contraseña</label>
						<input type="password" class="form-control input-sm" id="password" name="">
						<p></p>
						<span class="btn btn-primary" id="registrarNuevo">Registrar</span>
					</form>
					<!-- <div style="text-align: right;">
						<a href="index.php" class="btn btn-default">Login</a>
					</div> -->
					<div style="text-align: right;">
						<a href="https://www.th3code.com/propuesta/supershop/descarga.html" class="btn btn-default">Regresar</a>
					</div>
				</div>
			</div>
		</div>
		<div class="col-sm-4"></div>
	</div>
</div>
</body>
</html>

<script type="text/javascript">
	$(document).ready(function(){
		$('#registrarNuevo').click(function(){

			if($('#nombre').val()==""){
				alertify.alert("Ingrese nombre completo");
				return false;
			}else if($('#telefono').val()==""){
				alertify.alert("Ingrese telefono");
				return false;
			}else if($('#email').val()==""){
				alertify.alert("Ingrese correo");
				return false;
			}else if($('#calle_numero').val()==""){
				alertify.alert("Ingrese calle y numero ext");
				return false;
			}else if($('#colonia').val()==""){
				alertify.alert("Ingrese colonia");
				return false;
			}else if($('#usuario').val()==""){
				alertify.alert("Ingresa el usuario");
				return false;
			}else if($('#password').val()==""){
				alertify.alert("Ingresa el password");
				return false;
			}

			cadena="nombre=" + $('#nombre').val() +
					"&telefono=" + $('#telefono').val() +
					"&email=" + $('#email').val() +
					"&calleNumero=" + $('#calle_numero').val() +
					"&colonia=" + $('#colonia').val() +
					"&usuario=" + $('#usuario').val() + 
					"&password=" + $('#password').val();

					$.ajax({
						type:"POST",
						url:"CRUD/registro.php",
						data:cadena,
						success:function(r){
							if(r==1){
								$('#frmRegistro')[0].reset();
								alertify.success("Usuario agregado");
								setTimeout(() => {
									location.href ='http://th3code.com/propuesta/supershop/descarga.html';
								}, 500);
							}else{
								alertify.error("Falló al agregar");
							}
						}
					});
		});
	});
</script>

