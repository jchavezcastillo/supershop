-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Servidor: localhost:3306
-- Tiempo de generación: 28-04-2020 a las 13:56:09
-- Versión del servidor: 5.6.41-84.1
-- Versión de PHP: 7.2.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `chavezca_supershop`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `comentarios`
--

CREATE TABLE `comentarios` (
  `id` int(11) NOT NULL,
  `comentario` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `fecha` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `status` tinyint(4) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `detalle_pedidos`
--

CREATE TABLE `detalle_pedidos` (
  `id` int(11) NOT NULL,
  `pedido_id` int(11) NOT NULL,
  `nombre` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `imagen` varchar(600) COLLATE utf8_unicode_ci NOT NULL,
  `precio_normal` double(10,2) NOT NULL,
  `precio_descuento` double(10,2) NOT NULL,
  `cantidad` int(11) NOT NULL,
  `total` double(10,2) NOT NULL,
  `status` tinyint(4) NOT NULL,
  `fecha` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `detalle_pedidos`
--

INSERT INTO `detalle_pedidos` (`id`, `pedido_id`, `nombre`, `imagen`, `precio_normal`, `precio_descuento`, `cantidad`, `total`, `status`, `fecha`) VALUES
(5, 31, 'Lote de 200 encendedores ', '', 550.00, 400.00, 1, 400.00, 1, '2020-01-20 10:00:56'),
(6, 32, 'Lote de 200 encendedores ', '', 550.00, 400.00, 1, 400.00, 1, '2020-01-20 10:02:39'),
(7, 32, 'PAPEL OFIX MULTIFUNCIONAL ECO CARTA BLANCO 37K C/500HJS', '', 65.90, 0.00, 1, 65.90, 1, '2020-01-20 10:02:39'),
(8, 32, 'CARTULINA FACIA NEON MIX CARTA 5 COLORES C/100H', '', 131.50, 0.00, 1, 131.50, 1, '2020-01-20 10:02:39'),
(9, 33, 'PAPEL OFIX MULTIFUNCIONAL ECO CARTA BLANCO 37K C/500HJS', '', 65.90, 0.00, 1, 65.90, 1, '2020-01-20 16:48:22'),
(10, 33, 'CARTULINA FACIA NEON MIX CARTA 5 COLORES C/100H', '', 131.50, 0.00, 1, 131.50, 1, '2020-01-20 16:48:22'),
(11, 33, 'Lijadora orbital 1/4 Pulg 200W BO4556 Makita', '', 944.00, 0.00, 2, 1888.00, 1, '2020-01-20 16:48:22'),
(12, 34, 'Marca del Ahorro  Alcohol Azul 1 Lt', '', 48.00, 0.00, 4, 192.00, 1, '2020-01-20 16:48:51'),
(13, 35, 'Miniesmeriladora 4-1/2 Pulg 900W 1790 Bosch', '', 1416.00, 0.00, 2, 2832.00, 1, '2020-01-20 17:37:33'),
(14, 36, 'Lote de 200 encendedores ', '', 550.00, 400.00, 2, 800.00, 1, '2020-01-20 17:49:03'),
(15, 36, 'PAPEL OFIX MULTIFUNCIONAL ECO CARTA BLANCO 37K C/500HJS', '', 65.90, 0.00, 1, 65.90, 1, '2020-01-20 17:49:03'),
(16, 36, 'Miniesmeriladora 4-1/2 Pulg 900W 1790 Bosch', '', 1416.00, 0.00, 1, 1416.00, 1, '2020-01-20 17:49:03'),
(17, 36, 'Agua Oxigenada 480 ml Marca del Ahorro', '', 19.00, 0.00, 1, 19.00, 1, '2020-01-20 17:49:03'),
(18, 38, 'Bolsa de globos Blogos ', 'img/gomitas.jpeg', 550.00, 35.00, 1, 35.00, 1, '2020-01-31 18:43:37'),
(19, 38, 'Aceite para bebÃ©s 200 ml ', 'img/aceite-bebe.jpeg', 250.00, 200.00, 1, 200.00, 1, '2020-01-31 18:43:37'),
(20, 38, 'Flexi Arya 37903 Vino ', 'img/zapato_1_.jpg', 879.00, 679.00, 1, 679.00, 1, '2020-01-31 18:43:37'),
(21, 39, 'Zapatilla bordada ', 'img/zapato_3_3.jpg', 600.00, 500.00, 1, 500.00, 1, '2020-02-25 18:35:32'),
(22, 39, 'Puntera Redonda ', 'img/zapato_5.jpg', 621.00, 521.00, 1, 521.00, 1, '2020-02-25 18:35:32'),
(23, 40, 'Lote de 200 encendedores ', 'img/encendedor.jpeg', 550.00, 400.00, 2, 800.00, 1, '2020-02-25 20:16:15'),
(24, 41, 'Auxiliar contra la gastritis ', 'img/ofertas/omeprazol.jpeg', 550.00, 50.00, 1, 50.00, 1, '2020-02-26 14:59:04'),
(25, 41, 'Desinfectante verdura, fruta ', 'img/ofertas/decinfectante-verdura.jpeg', 636.74, 100.00, 3, 300.00, 1, '2020-02-26 14:59:04'),
(26, 41, 'Spray fijador Caprice ', 'img/ofertas/caprice.jpeg', 550.00, 30.00, 3, 90.00, 1, '2020-02-26 14:59:04'),
(27, 42, 'Auxiliar contra la gastritis ', 'img/ofertas/omeprazol.jpeg', 550.00, 50.00, 2, 100.00, 1, '2020-02-26 15:01:58'),
(28, 0, 'Acetona pris caja con 100pz ', 'img/ofertas/acetona.jpeg', 250.00, 760.00, 1, 760.00, 1, '2020-02-26 19:54:12'),
(29, 0, 'Paracetamol ', 'img/ofertas/Paracetamol.jpeg', 105.00, 75.00, 1, 75.00, 1, '2020-02-26 19:54:12'),
(30, 0, 'Paracetamol ', 'img/ofertas/Paracetamol.jpeg', 105.00, 75.00, 1, 75.00, 1, '2020-02-26 19:56:11'),
(31, 0, 'Acetona pris caja con 100pz ', 'img/ofertas/acetona.jpeg', 250.00, 760.00, 1, 760.00, 1, '2020-02-26 19:56:11'),
(32, 43, 'Spray fijador Caprice ', 'img/ofertas/caprice.jpeg', 550.00, 30.00, 1, 30.00, 1, '2020-02-26 19:57:20'),
(33, 43, 'Acetona pris caja con 100pz ', 'img/ofertas/acetona.jpeg', 250.00, 760.00, 1, 760.00, 1, '2020-02-26 19:57:20'),
(34, 44, 'Spray fijador Caprice ', 'img/ofertas/caprice.jpeg', 550.00, 30.00, 1, 30.00, 1, '2020-02-26 20:12:41'),
(35, 45, 'Auxiliar contra la gastritis ', 'img/ofertas/omeprazol.jpeg', 550.00, 50.00, 4, 200.00, 1, '2020-03-31 15:08:41'),
(36, 45, 'Spray fijador Caprice ', 'img/ofertas/caprice.jpeg', 550.00, 30.00, 3, 90.00, 1, '2020-03-31 15:08:41'),
(37, 46, 'Desinfectante verdura, fruta ', 'img/ofertas/decinfectante-verdura.jpeg', 636.74, 100.00, 4, 400.00, 1, '2020-03-31 16:49:47'),
(38, 46, 'Zapatilla bordada ', 'img/zapato_3_3.jpg', 600.00, 500.00, 4, 2000.00, 1, '2020-03-31 16:49:47'),
(39, 46, 'Perugia Tacones ', 'img/zapato_4.jpg', 499.00, 399.00, 2, 798.00, 1, '2020-03-31 16:49:47'),
(40, 47, '1 ', 'img/articulos/frutas-verduras/22.jpeg', 1.00, 0.00, 1, 1.00, 1, '2020-04-05 10:20:05'),
(41, 47, '3 ', 'img/articulos/frutas-verduras/4.jpeg', 1.00, 0.00, 1, 1.00, 1, '2020-04-05 10:20:05'),
(42, 48, '1 ', 'img/articulos/frutas-verduras/22.jpeg', 1.00, 0.00, 1, 1.00, 1, '2020-04-05 10:40:06'),
(43, 48, '3 ', 'img/articulos/frutas-verduras/4.jpeg', 1.00, 0.00, 1, 1.00, 1, '2020-04-05 10:40:06'),
(44, 48, '4 ', 'img/articulos/frutas-verduras/1.jpeg', 1.00, 0.00, 1, 1.00, 1, '2020-04-05 10:40:06'),
(45, 49, '1 ', 'img/articulos/frutas-verduras/22.jpeg', 1.00, 0.00, 1, 1.00, 1, '2020-04-05 11:20:17'),
(46, 49, '3 ', 'img/articulos/frutas-verduras/4.jpeg', 1.00, 0.00, 1, 1.00, 1, '2020-04-05 11:20:17'),
(47, 49, '4 ', 'img/articulos/frutas-verduras/1.jpeg', 1.00, 0.00, 1, 1.00, 1, '2020-04-05 11:20:17'),
(48, 50, '1 ', 'img/articulos/frutas-verduras/22.jpeg', 1.00, 0.00, 1, 1.00, 1, '2020-04-05 11:36:48'),
(49, 50, '3 ', 'img/articulos/frutas-verduras/4.jpeg', 1.00, 0.00, 1, 1.00, 1, '2020-04-05 11:36:48'),
(50, 50, '4 ', 'img/articulos/frutas-verduras/1.jpeg', 1.00, 0.00, 1, 1.00, 1, '2020-04-05 11:36:48'),
(51, 50, '6 ', 'img/articulos/frutas-verduras/3.jpeg', 1.00, 0.00, 5, 1.00, 1, '2020-04-05 11:36:48'),
(52, 0, '1 ', 'img/articulos/frutas-verduras/22.jpeg', 1.00, 0.00, 1, 1.00, 1, '2020-04-05 19:24:19'),
(53, 0, '3 ', 'img/articulos/frutas-verduras/4.jpeg', 1.00, 0.00, 1, 1.00, 1, '2020-04-05 19:24:19'),
(54, 0, '4 ', 'img/articulos/frutas-verduras/1.jpeg', 1.00, 0.00, 1, 1.00, 1, '2020-04-05 19:24:19'),
(55, 0, '5 ', 'img/articulos/frutas-verduras/2.jpeg', 1.00, 0.00, 1, 1.00, 1, '2020-04-05 19:24:19'),
(56, 0, '6 ', 'img/articulos/frutas-verduras/3.jpeg', 1.00, 0.00, 1, 1.00, 1, '2020-04-05 19:24:19'),
(57, 0, '6 ', 'img/articulos/frutas-verduras/8.jpeg', 1.00, 0.00, 1, 1.00, 1, '2020-04-05 19:24:19'),
(58, 0, '1 ', 'img/articulos/frutas-verduras/22.jpeg', 1.00, 0.00, 1, 1.00, 1, '2020-04-05 19:28:01'),
(59, 0, '3 ', 'img/articulos/frutas-verduras/4.jpeg', 1.00, 0.00, 1, 1.00, 1, '2020-04-05 19:28:01'),
(60, 0, '4 ', 'img/articulos/frutas-verduras/1.jpeg', 1.00, 0.00, 1, 1.00, 1, '2020-04-05 19:28:01'),
(61, 0, '5 ', 'img/articulos/frutas-verduras/2.jpeg', 1.00, 0.00, 1, 1.00, 1, '2020-04-05 19:28:01'),
(62, 0, '6 ', 'img/articulos/frutas-verduras/3.jpeg', 1.00, 0.00, 1, 1.00, 1, '2020-04-05 19:28:01'),
(63, 0, '6 ', 'img/articulos/frutas-verduras/8.jpeg', 1.00, 0.00, 1, 1.00, 1, '2020-04-05 19:28:01'),
(64, 0, '7 ', 'img/articulos/frutas-verduras/9.jpeg', 1.00, 0.00, 1, 1.00, 1, '2020-04-05 19:28:01'),
(65, 0, '1 ', 'img/articulos/frutas-verduras/22.jpeg', 1.00, 0.00, 1, 1.00, 1, '2020-04-05 19:28:51'),
(66, 0, '1 ', 'img/articulos/frutas-verduras/22.jpeg', 1.00, 0.00, 1, 1.00, 1, '2020-04-05 19:31:49'),
(67, 0, '3 ', 'img/articulos/frutas-verduras/4.jpeg', 1.00, 0.00, 1, 1.00, 1, '2020-04-05 19:31:49'),
(68, 0, '4 ', 'img/articulos/frutas-verduras/1.jpeg', 1.00, 0.00, 1, 1.00, 1, '2020-04-05 19:31:49'),
(69, 0, '5 ', 'img/articulos/frutas-verduras/2.jpeg', 1.00, 0.00, 1, 1.00, 1, '2020-04-05 19:31:49'),
(70, 0, '6 ', 'img/articulos/frutas-verduras/3.jpeg', 1.00, 0.00, 1, 1.00, 1, '2020-04-05 19:31:49'),
(71, 0, '1 ', 'img/articulos/frutas-verduras/22.jpeg', 1.00, 0.00, 1, 1.00, 1, '2020-04-05 19:34:03'),
(72, 0, '1 ', 'img/articulos/frutas-verduras/22.jpeg', 1.00, 0.00, 1, 1.00, 1, '2020-04-05 19:37:31'),
(73, 0, '3 ', 'img/articulos/frutas-verduras/4.jpeg', 1.00, 0.00, 1, 1.00, 1, '2020-04-05 19:37:31'),
(74, 51, '1 ', 'img/articulos/frutas-verduras/22.jpeg', 1.00, 0.00, 1, 1.00, 1, '2020-04-05 19:44:31'),
(75, 51, '3 ', 'img/articulos/frutas-verduras/4.jpeg', 1.00, 0.00, 1, 1.00, 1, '2020-04-05 19:44:31'),
(76, 51, '4 ', 'img/articulos/frutas-verduras/1.jpeg', 1.00, 0.00, 1, 1.00, 1, '2020-04-05 19:44:31'),
(77, 52, 'Uva verde ', 'img/articulos/frutas-verduras/9.jpeg', 30.00, 0.00, 4, 120.00, 1, '2020-04-06 13:09:39'),
(78, 54, 'manzana ', 'img/articulos/frutas-verduras/22.jpeg', 40.00, 0.00, 1, 40.00, 1, '2020-04-07 13:32:27'),
(79, 54, 'Ajo ', 'img/articulos/frutas-verduras/4.jpeg', 10.00, 0.00, 3, 10.00, 1, '2020-04-07 13:32:27'),
(80, 54, 'Uva ', 'img/articulos/frutas-verduras/1.jpeg', 30.00, 0.00, 3, 30.00, 1, '2020-04-07 13:32:27'),
(81, 56, 'Advil Max ', 'img/articulos/material-curacion/1.png', 0.00, 0.00, 5, 0.00, 1, '2020-04-27 21:36:29'),
(82, 56, 'Advil Ibuprofeno ', 'img/articulos/material-curacion/2.png', 0.00, 0.00, 5, 0.00, 1, '2020-04-27 21:36:29'),
(83, 56, 'manzana ', 'img/articulos/frutas-verduras/22.jpeg', 50.00, 0.00, 1, 62.50, 1, '2020-04-27 21:36:29'),
(84, 56, 'Ajo ', 'img/articulos/frutas-verduras/4.jpeg', 15.00, 0.00, 1, 7.50, 1, '2020-04-27 21:36:29');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ofertas`
--

CREATE TABLE `ofertas` (
  `id` int(11) NOT NULL,
  `nombre` varchar(300) COLLATE utf8_unicode_ci NOT NULL,
  `url` varchar(600) COLLATE utf8_unicode_ci NOT NULL,
  `fila` int(2) NOT NULL,
  `descripcion` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `precio_anterior` double(5,2) NOT NULL,
  `precio_actual` double(5,2) NOT NULL,
  `existencia` double(5,2) NOT NULL,
  `status` tinyint(1) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `ofertas`
--

INSERT INTO `ofertas` (`id`, `nombre`, `url`, `fila`, `descripcion`, `precio_anterior`, `precio_actual`, `existencia`, `status`) VALUES
(1, 'Auxiliar contra la gastritis', 'img/ofertas/omeprazol.jpeg', 1, 'Medicamento', 60.00, 50.00, 200.00, 1),
(2, 'Spray fijador Caprice', 'img/ofertas/caprice.jpeg', 1, 'Fijador', 40.00, 30.00, 200.00, 1),
(3, 'Acetona pris caja con 100pz', 'img/ofertas/acetona.jpeg', 1, 'Acetona', 860.00, 760.00, 200.00, 1),
(4, 'Paracetamol analg&eacute;sico', 'img/ofertas/Paracetamol.jpeg', 1, 'Analg&eacute;sico', 105.00, 75.00, 200.00, 1),
(5, 'Desinfectante verdura, fruta', 'img/ofertas/decinfectante-verdura.jpeg', 2, 'Desinfectante', 130.00, 100.00, 200.00, 1),
(6, 'Zapatilla bordada', 'img/zapato_3_3.jpg', 2, 'Zapatos', 600.00, 500.00, 200.00, 1),
(7, 'Puntera Redonda', 'img/zapato_5.jpg', 2, 'Zapatos', 621.00, 521.00, 200.00, 1),
(8, 'Perugia Tacones', 'img/zapato_4.jpg', 2, 'Zapatos', 499.00, 399.00, 200.00, 1),
(9, 'Esprit Tiffany Bota', 'img/zapato_2_.jpg', 3, 'Zapatos', 636.74, 536.74, 200.00, 1),
(10, 'Bolsa de globos Blogos', 'img/gomitas.jpeg', 3, 'Dulces', 40.00, 35.00, 200.00, 1),
(11, 'Zapatilla bordada', 'img/zapato_3_3.jpg', 3, 'Zapatos', 600.00, 500.00, 200.00, 1),
(12, 'Perugia Tacones', 'img/zapato_4.jpg', 3, 'Zapatos', 499.00, 399.00, 200.00, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pedidos`
--

CREATE TABLE `pedidos` (
  `id` int(11) NOT NULL,
  `usuario` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `total` double(6,2) NOT NULL,
  `forma_pago` int(2) NOT NULL COMMENT 'Donde 1 = Contra entrega, 2 = Pago con tarjeta débito o crédito, 3 = Pago en tienda OXXO, 4 = Transferencia interbancaria BBVA',
  `lista_producto` text COLLATE utf8_unicode_ci NOT NULL,
  `presupuesto_mandado` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `status` tinyint(4) NOT NULL,
  `fecha` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `pedidos`
--

INSERT INTO `pedidos` (`id`, `usuario`, `total`, `forma_pago`, `lista_producto`, `presupuesto_mandado`, `status`, `fecha`) VALUES
(42, '2283041691 ', 100.00, 1, '', '', 1, '2020-02-26 20:13:00'),
(41, '2283041691 ', 440.00, 1, '', '', 1, '2020-02-26 20:13:05'),
(40, '2283041691', 800.00, 1, '', '', 1, '2020-02-26 20:13:09'),
(39, '2281895262', 1021.00, 1, '', '', 4, '2020-04-08 14:57:57'),
(38, '2281895262', 914.00, 1, '', '', 3, '2020-02-26 20:13:15'),
(37, '2281895262', 679.00, 1, '', '', 1, '2020-02-26 20:13:18'),
(36, 'rafa', 2300.90, 1, '', '', 2, '2020-02-26 20:13:21'),
(35, '2281895262', 2832.00, 1, '', '', 3, '2020-02-26 20:13:24'),
(34, '2281895262', 192.00, 1, '', '', 1, '2020-02-26 20:13:27'),
(33, '2281042014', 2085.40, 1, '', '', 1, '2020-02-26 20:13:30'),
(32, '2281895262', 597.40, 1, '', '', 1, '2020-02-26 20:13:33'),
(31, '2281895262', 597.40, 1, '', '', 2, '2020-02-26 20:13:36'),
(30, '2281895262', 1129.20, 1, '', '', 3, '2020-02-26 20:13:38'),
(43, '2281895262', 790.00, 4, '', '', 1, '2020-02-26 19:57:20'),
(44, '2281895262', 30.00, 2, '', '', 1, '2020-02-26 20:12:41'),
(45, '2281042014', 290.00, 1, '', '', 1, '2020-03-31 15:08:41'),
(46, '2281042014', 3198.00, 4, '', '', 1, '2020-03-31 16:49:47'),
(47, '', 2.00, 1, '', '', 1, '2020-04-05 10:20:05'),
(48, '2281895262', 3.00, 3, '', '', 1, '2020-04-05 10:40:06'),
(49, '2281895262', 3.00, 2, '', '', 1, '2020-04-05 11:20:17'),
(50, '2281895262', 4.00, 4, '', '', 1, '2020-04-05 11:36:48'),
(51, '2281895262', 3.00, 3, 'lista producto', '100', 1, '2020-04-05 19:44:31'),
(52, '2281917206', 120.00, 2, 'CafÃ© para cafetera', '500', 1, '2020-04-06 13:09:39'),
(53, '', 0.00, 2, '', '', 1, '2020-04-06 19:59:34'),
(54, '2281895382', 80.00, 1, '', '', 1, '2020-04-07 13:32:27'),
(55, '', 0.00, 3, 'text', 'text', 1, '2020-04-08 20:36:45'),
(56, '2281895262', 70.00, 1, '- JabÃ³n en polvo.\n- Frijol la siella 400 gr', '300', 1, '2020-04-27 21:36:29');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `productos`
--

CREATE TABLE `productos` (
  `id` int(11) NOT NULL,
  `nombre` varchar(300) CHARACTER SET utf8 NOT NULL,
  `categoria` int(1) NOT NULL COMMENT 'Donde 1= curación, 2 = fereteria, 3 = Papeleria, 4 = zona oportunidades, 5 = varios, 6 = Cosas de bebé, 7 = Higiene personal, Frutas y verduras = 8',
  `url` varchar(600) COLLATE utf8_unicode_ci NOT NULL,
  `precio` double(10,2) NOT NULL,
  `precio_app` double(10,2) NOT NULL,
  `existencias` double(10,3) NOT NULL,
  `cantidad_minima` double(10,3) NOT NULL,
  `unidad_medida` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `productos`
--

INSERT INTO `productos` (`id`, `nombre`, `categoria`, `url`, `precio`, `precio_app`, `existencias`, `cantidad_minima`, `unidad_medida`, `status`) VALUES
(1, 'Advil Max', 1, 'img/articulos/material-curacion/1.png', 75.00, 0.00, 200.000, 1.000, 'pz', 1),
(2, 'Advil Ibuprofeno', 1, 'img/articulos/material-curacion/2.png', 44.00, 0.00, 200.000, 1.000, 'pz', 1),
(3, 'Aspirina Efervescente 500mg', 1, 'img/articulos/material-curacion/3.png', 144.00, 0.00, 200.000, 1.000, 'pz', 1),
(4, 'Aspirina', 1, 'img/articulos/material-curacion/4.png', 85.00, 0.00, 200.000, 1.000, 'pz', 1),
(5, 'Bi Electro', 1, 'img/articulos/material-curacion/5.png', 70.00, 0.00, 200.000, 1.000, 'pz', 1),
(6, 'Brysbal Gel Pharma', 1, 'img/articulos/material-curacion/6.png', 38.00, 0.00, 200.000, 1.000, 'pz', 1),
(7, 'CafiAspirina Forte 24 tabletas', 1, 'img/articulos/material-curacion/7.png', 53.00, 0.00, 200.000, 1.000, 'pz', 1),
(8, 'CafiAspirina 100 tabletas', 1, 'img/articulos/material-curacion/8.png', 103.00, 0.00, 200.000, 1.000, 'pz', 1),
(9, 'Dolver 400mg', 1, 'img/articulos/material-curacion/9.png', 23.00, 0.00, 200.000, 1.000, 'pz', 1),
(10, 'Dolver 800mg', 1, 'img/articulos/material-curacion/10.png', 44.00, 0.00, 200.000, 1.000, 'pz', 1),
(11, 'Dualgos Paracetamol 20 tabletas', 1, 'img/articulos/material-curacion/11.png', 37.00, 0.00, 200.000, 1.000, 'pz', 1),
(12, '	\r\nKetorolaco DMI', 1, 'img/articulos/material-curacion/12.png', 38.00, 0.00, 200.000, 1.000, 'pz', 1),
(13, 'Magnopyrol 500mg Columbia', 1, 'img/articulos/material-curacion/13.png', 53.00, 0.00, 200.000, 1.000, 'pz', 1),
(14, 'Ketorolaco Lorotec', 1, 'img/articulos/material-curacion/14.png', 50.00, 0.00, 200.000, 1.000, 'pz', 1),
(15, 'Mejoral 12 tabletas', 1, 'img/articulos/material-curacion/15.png', 38.00, 0.00, 200.000, 1.000, 'pz', 1),
(16, 'Paracetamol 10 tabletas Hormons', 1, 'img/articulos/material-curacion/16.png', 20.00, 0.00, 200.000, 1.000, 'pz', 1),
(17, 'Paracetamol y Cafeína Saridon', 1, 'img/articulos/material-curacion/17.png', 235.00, 0.00, 200.000, 1.000, 'pz', 1),
(18, 'Tempra 20 tabletas', 1, 'img/articulos/material-curacion/18.png', 69.00, 0.00, 200.000, 1.000, 'pz', 1),
(19, 'Tempra frote 20 tabletas', 1, 'img/articulos/material-curacion/19.png', 88.00, 0.00, 200.000, 1.000, 'pz', 1),
(20, 'Keterolaco TR 10 cápsulas Mavidol', 1, 'img/articulos/material-curacion/20.png', 53.00, 0.00, 200.000, 1.000, 'pz', 1),
(21, 'Keterolaco 10 tabletas Marvidol', 1, 'img/articulos/material-curacion/21.png', 25.00, 0.00, 200.000, 1.000, 'pz', 1),
(87, 'manzana', 8, 'img/articulos/frutas-verduras/22.jpeg', 40.00, 50.00, 20.000, 0.250, 'kg', 1),
(69, 'Ajo', 8, 'img/articulos/frutas-verduras/4.jpeg', 10.00, 15.00, 20.000, 0.250, 'kg', 1),
(42, 'Uva', 8, 'img/articulos/frutas-verduras/1.jpeg', 30.00, 35.00, 5.000, 0.250, 'kg', 1),
(43, 'CamarÃ³n rojo', 10, 'img/articulos/frutas-verduras/2.jpeg', 25.00, 3.00, 50.000, 0.250, 'kg', 1),
(44, 'CamarÃ³n blanco', 10, 'img/articulos/frutas-verduras/3.jpeg', 20.00, 25.00, 10.000, 0.250, 'kg', 1),
(73, 'PiÃ±a', 8, 'img/articulos/frutas-verduras/8.jpeg', 60.00, 70.00, 2.000, 0.250, 'kg', 1),
(74, 'Uva verde', 8, 'img/articulos/frutas-verduras/9.jpeg', 30.00, 35.00, 4.000, 0.250, 'kg', 1),
(75, 'Durazno', 8, 'img/articulos/frutas-verduras/10.jpeg', 50.00, 60.00, 3.000, 0.250, 'kg', 1),
(76, 'MelÃ³n', 8, 'img/articulos/frutas-verduras/11.jpeg', 45.00, 50.00, 4.000, 0.250, 'kg', 1),
(77, 'Chile rojo', 8, 'img/articulos/frutas-verduras/12.jpeg', 12.00, 18.00, 3.000, 0.250, 'kg', 1),
(78, 'Chile amarillo', 8, 'img/articulos/frutas-verduras/13.jpeg', 12.00, 18.00, 3.000, 0.250, 'kg', 1),
(79, 'Pepino', 8, 'img/articulos/frutas-verduras/14.jpeg', 20.00, 25.00, 2.500, 0.250, 'kg', 1),
(80, 'Aguacate', 8, 'img/articulos/frutas-verduras/15.jpeg', 30.00, 40.00, 6.000, 0.250, 'kg', 1),
(81, 'Papa', 8, 'img/articulos/frutas-verduras/16.jpeg', 28.00, 34.00, 1.000, 0.250, 'kg', 1),
(82, 'LimÃ³n', 8, 'img/articulos/frutas-verduras/17.jpeg', 15.00, 19.00, 3.000, 0.250, 'kg', 1),
(83, 'Lechuga', 8, 'img/articulos/frutas-verduras/18.jpeg', 10.00, 12.00, 200.000, 0.250, 'kg', 1),
(84, 'Brocoli', 8, 'img/articulos/frutas-verduras/19.jpeg', 12.00, 15.00, 10.000, 0.250, 'kg', 1),
(85, 'Manzana verde', 8, 'img/articulos/frutas-verduras/20.jpeg', 25.00, 30.00, 6.000, 0.250, 'kg', 1),
(70, 'Pepino', 8, 'img/articulos/frutas-verduras/5.jpeg', 18.00, 24.00, 8.000, 0.250, 'kg', 1),
(71, 'borrar', 8, 'img/articulos/frutas-verduras/6.jpeg', 1.00, 0.00, 200.000, 0.250, 'kg', 1),
(72, 'Papaya', 8, 'img/articulos/frutas-verduras/7.jpeg', 40.00, 55.00, 12.000, 0.250, 'kg', 1),
(88, 'Brocoli blanco', 8, 'img/articulos/frutas-verduras/23.jpeg', 26.00, 30.00, 9.000, 0.250, 'kg', 1),
(89, 'PlÃ¡tano', 8, 'img/articulos/frutas-verduras/24.jpeg', 8.00, 12.00, 9.000, 0.250, 'kg', 1),
(90, '24', 8, 'img/articulos/frutas-verduras/25.jpeg', 1.00, 0.00, 200.000, 0.250, 'kg', 1),
(91, '25', 8, 'img/articulos/frutas-verduras/26.jpeg', 1.00, 0.00, 200.000, 0.250, 'kg', 1),
(92, 'Chile', 8, 'img/articulos/frutas-verduras/27.jpeg', 8.00, 15.00, 5.000, 0.250, 'kg', 1),
(93, 'Pera', 8, 'img/articulos/frutas-verduras/28.jpeg', 29.00, 34.00, 12.000, 0.250, 'kg', 1),
(94, '28', 8, 'img/articulos/frutas-verduras/29.jpeg', 1.00, 0.00, 200.000, 0.250, 'kg', 1),
(95, '29', 8, 'img/articulos/frutas-verduras/30.jpeg', 1.00, 0.00, 200.000, 0.250, 'kg', 1),
(96, 'borrar', 8, 'img/articulos/frutas-verduras/31.jpeg', 1.00, 0.00, 200.000, 0.250, 'kg', 0),
(97, 'Fresa', 8, 'img/articulos/frutas-verduras/32.jpeg', 30.00, 34.00, 8.000, 0.250, 'kg', 1),
(98, '...', 8, 'img/articulos/frutas-verduras/33.jpeg', 1.00, 0.00, 200.000, 0.250, 'kg', 1),
(99, '...', 8, 'img/articulos/frutas-verduras/34.jpeg', 1.00, 0.00, 200.000, 0.250, 'kg', 1),
(100, 'borrar', 8, 'img/articulos/frutas-verduras/35.jpeg', 1.00, 0.00, 200.000, 0.250, 'kg', 1),
(101, 'borrar', 8, 'img/articulos/frutas-verduras/36.jpeg', 1.00, 0.00, 200.000, 0.250, 'kg', 1),
(102, 'Zanahoria', 8, 'img/articulos/frutas-verduras/37.jpeg', 20.00, 25.00, 10.000, 0.250, 'kg', 1),
(103, 'borrar', 8, 'img/articulos/frutas-verduras/38.jpeg', 1.00, 0.00, 200.000, 0.250, 'kg', 1),
(104, 'Elote', 8, 'img/articulos/frutas-verduras/39.jpeg', 15.00, 19.00, 4.000, 0.250, 'kg', 1),
(105, 'SandÃ­a ', 8, 'img/articulos/frutas-verduras/melon.jpeg', 50.00, 60.00, 7.000, 0.250, 'kg', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuarios`
--

CREATE TABLE `usuarios` (
  `id_cliente` int(11) NOT NULL,
  `nombre` varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL,
  `telefono` bigint(20) DEFAULT NULL,
  `email` varchar(300) COLLATE utf8_unicode_ci DEFAULT NULL,
  `calle_numero` varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL,
  `colonia` varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL,
  `usuario` varchar(300) COLLATE utf8_unicode_ci DEFAULT NULL,
  `password` varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `usuarios`
--

INSERT INTO `usuarios` (`id_cliente`, `nombre`, `telefono`, `email`, `calle_numero`, `colonia`, `usuario`, `password`) VALUES
(2, 'Rafael Hdez', 2281102030, 'rafael@gmail.com', 'Allende 51', 'Centro', 'rafa', '7110eda4d09e062aa5e4a390b0a572ac0d2c0220'),
(5, 'AndrÃ©s mancera barradas', 2281042014, 'morvidos@hotmail.com', 'Ignacio Aldama', 'centro', '2281042014', 'e411a490148911bb6ee16beaa6f794a437b3bc9d'),
(6, 'JosÃ© Rafael Mancera Barradas ', 2283041691, 'czarnio@live.com', '20 de noviembre 229 A ', 'JosÃ© Cardel ', 'Czarnio', '987639c41b3593e400bbe3ae5120ec7a3ae875ad'),
(7, 'Bleising Javier', 2281400555, 'bleising_javier@hotmail.com', 'Av. Miguel AlemÃ¡n 172R Altos', 'Hidalgo', 'BleyJav', 'd3497fa6a8e8a5b773e9cfcb4b434155284c881b'),
(8, 'Edwin Castillo', 2281761073, 'bleisingjavier123@gmail.com', 'Miguel alemÃ¡n 172R', 'Hidalgo', 'Edwin08', 'd3497fa6a8e8a5b773e9cfcb4b434155284c881b'),
(9, 'Q', 33, '2323@44@GMAIL.COM', '2DD', 'SD', 'JUANPEREZPEREZ', '7110eda4d09e062aa5e4a390b0a572ac0d2c0220'),
(10, 'Alejandra ', 2283041185, 'czarnio@live.com', '20 de noviembre 102', 'Centro', 'Czarnio', '987639c41b3593e400bbe3ae5120ec7a3ae875ad'),
(11, 'Usuario Prueba ', 2288228822, 'Jiji_789@gmail.com', 'Pipila #457', 'Pipila', 'Usuario P', '7c222fb2927d828af22f592134e8932480637c0d'),
(12, 'usuario de prueba', 2222222, 'usuariodeprueba@yopmail.com', 'Octavio Blancas', 'Fraccionamiento Lucas martÃ­n', 'usuarioprueba', '7110eda4d09e062aa5e4a390b0a572ac0d2c0220'),
(14, 'Lola loili', 226742236, 'Wbbjyd_thj@gmail.com', 'MÃ©xico 9929', 'Para alla', 'Prueb', 'f92c43ac98393d1e2dded0156fe15d0139f1a4b2'),
(15, 'JosÃ© Rafael Mancera ', 2283041691, 'czarnio@live.com', '20 de noviembre 229 A ', 'Centro', 'Czarnio', '987639c41b3593e400bbe3ae5120ec7a3ae875ad'),
(16, 'Gerzon Garcia Arcos', 2281403919, 'Gerzon30@gmail.com', 'Santa MarÃ­a 16 A', 'Lomas de santa fe', 'GerzonGarcia', '8cb2237d0679ca88db6464eac60da96345513964'),
(26, 'Artemisa GonzÃ¡lez', 2281917206, 'Corazonvaliente23@gmail.com', 'Poniente 4 no 75', 'Ferrocarrilera', '2281917206', '0430bff2c2586383009db9fbe50d77f6c2428d78'),
(19, 'AndrÃ©s mancera barradas', 2281042014, 'morvidos@hotmail.com ', 'ArcÃ¡ngel San Rafael no 4', 'La herradura', 'Morvidos', '5316e7548441e4b4da856de7a83e5c89feee000d'),
(25, 'Jhonatan probando final', 2281895262, 'jj@th3code.com', 'Octavio Blancas.', 'Fraccionamiento Lucas martÃ­n.', '2281895262', '984e18fe201c8f99e0e443707464d5b5ffaf326a'),
(23, 'Jhonatan probando final', 2281895262, 'jj@th3code.com', 'Octavio Blancas.', 'Fraccionamiento Lucas martÃ­n.', '2281895262', '984e18fe201c8f99e0e443707464d5b5ffaf326a');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `comentarios`
--
ALTER TABLE `comentarios`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `detalle_pedidos`
--
ALTER TABLE `detalle_pedidos`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `ofertas`
--
ALTER TABLE `ofertas`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `pedidos`
--
ALTER TABLE `pedidos`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `productos`
--
ALTER TABLE `productos`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  ADD PRIMARY KEY (`id_cliente`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `detalle_pedidos`
--
ALTER TABLE `detalle_pedidos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=85;

--
-- AUTO_INCREMENT de la tabla `ofertas`
--
ALTER TABLE `ofertas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT de la tabla `pedidos`
--
ALTER TABLE `pedidos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=57;

--
-- AUTO_INCREMENT de la tabla `productos`
--
ALTER TABLE `productos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=106;

--
-- AUTO_INCREMENT de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  MODIFY `id_cliente` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
