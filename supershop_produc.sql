-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 21-01-2020 a las 00:52:07
-- Versión del servidor: 10.4.10-MariaDB
-- Versión de PHP: 7.1.33

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `supershop_produc`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `detalle_pedidos`
--

CREATE TABLE `detalle_pedidos` (
  `id` int(11) NOT NULL,
  `pedido_id` int(11) NOT NULL,
  `imagen` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `nombre` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `precio_normal` double(10,2) NOT NULL,
  `precio_descuento` double(10,2) NOT NULL,
  `cantidad` int(11) NOT NULL,
  `total` double(10,2) NOT NULL,
  `status` tinyint(4) NOT NULL,
  `fecha` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `detalle_pedidos`
--

INSERT INTO `detalle_pedidos` (`id`, `pedido_id`, `imagen`, `nombre`, `precio_normal`, `precio_descuento`, `cantidad`, `total`, `status`, `fecha`) VALUES
(5, 31, 'img/encendedor.jpeg', 'Lote de 200 encendedores ', 550.00, 400.00, 1, 400.00, 1, '2020-01-20 22:27:15'),
(6, 32, 'img/encendedor.jpeg', 'Lote de 200 encendedores ', 550.00, 400.00, 1, 400.00, 1, '2020-01-20 22:27:18'),
(7, 32, 'img/articulos/papeleria/papel-carta.jpeg', 'PAPEL OFIX MULTIFUNCIONAL ECO CARTA BLANCO 37K C/500HJS', 65.90, 0.00, 1, 65.90, 1, '2020-01-20 22:25:03'),
(8, 32, 'img/articulos/papeleria/cartulina.jpg', 'CARTULINA FACIA NEON MIX CARTA 5 COLORES C/100H', 131.50, 0.00, 1, 131.50, 1, '2020-01-20 22:25:44'),
(9, 33, 'img/articulos/papeleria/papel-carta.jpeg', 'PAPEL OFIX MULTIFUNCIONAL ECO CARTA BLANCO 37K C/500HJS', 65.90, 0.00, 1, 65.90, 1, '2020-01-20 22:24:56'),
(10, 33, 'img/articulos/papeleria/cartulina.jpg', 'CARTULINA FACIA NEON MIX CARTA 5 COLORES C/100H', 131.50, 0.00, 1, 131.50, 1, '2020-01-20 22:25:39'),
(11, 33, 'img/articulos/ferreteria/lijadora.jpg', 'Lijadora orbital 1/4 Pulg 200W BO4556 Makita', 944.00, 0.00, 2, 1888.00, 1, '2020-01-20 22:31:26'),
(12, 34, 'img/articulos/material-curacion/alcohol.jpg', 'Marca del Ahorro  Alcohol Azul 1 Lt', 48.00, 0.00, 4, 192.00, 1, '2020-01-20 22:32:16'),
(13, 35, 'img/articulos/ferreteria/mini-esmeril.jpg', 'Miniesmeriladora 4-1/2 Pulg 900W 1790 Bosch', 1416.00, 0.00, 2, 2832.00, 1, '2020-01-20 22:30:39'),
(14, 36, 'img/articulos/papeleria/papel-carta.jpeg', 'PAPEL OFIX MULTIFUNCIONAL ECO CARTA BLANCO 37K C/500HJS', 65.90, 0.00, 2, 131.80, 1, '2020-01-20 22:24:44'),
(15, 36, 'img/encendedor.jpeg', 'Lote de 200 encendedores ', 550.00, 400.00, 2, 800.00, 1, '2020-01-20 22:27:25'),
(16, 37, 'img/articulos/papeleria/papel-carta.jpeg', 'PAPEL OFIX MULTIFUNCIONAL ECO CARTA BLANCO 37K C/500HJS', 65.90, 0.00, 1, 65.90, 1, '2020-01-20 20:01:54'),
(17, 37, 'img/articulos/papeleria/cartulina.jpg', 'CARTULINA FACIA NEON MIX CARTA 5 COLORES C/100H', 131.50, 0.00, 1, 131.50, 1, '2020-01-20 20:02:07'),
(18, 39, 'img/encendedor.jpeg', 'Lote de 200 encendedores ', 550.00, 400.00, 1, 400.00, 1, '2020-01-20 22:36:18'),
(19, 39, 'img/articulos/papeleria/papel-carta.jpeg', 'PAPEL OFIX MULTIFUNCIONAL ECO CARTA BLANCO 37K C/500HJS', 65.90, 0.00, 1, 65.90, 1, '2020-01-20 22:36:22'),
(20, 39, 'img/articulos/papeleria/cartulina.jpg', 'CARTULINA FACIA NEON MIX CARTA 5 COLORES C/100H', 131.50, 0.00, 1, 131.50, 1, '2020-01-20 22:36:25'),
(21, 39, 'img/articulos/ferreteria/lijadora.jpg', 'Lijadora orbital 1/4 Pulg 200W BO4556 Makita', 944.00, 0.00, 1, 944.00, 1, '2020-01-20 22:36:28'),
(22, 39, 'img/articulos/ferreteria/mini-esmeril.jpg', 'Miniesmeriladora 4-1/2 Pulg 900W 1790 Bosch', 1416.00, 0.00, 1, 1416.00, 1, '2020-01-20 22:36:31'),
(23, 39, 'img/articulos/material-curacion/alcohol.jpg', 'Marca del Ahorro  Alcohol Azul 1 Lt', 48.00, 0.00, 1, 48.00, 1, '2020-01-20 22:36:34'),
(24, 39, 'img/articulos/material-curacion/oxigenada.jpg', 'Agua Oxigenada 480 ml Marca del Ahorro', 19.00, 0.00, 1, 19.00, 1, '2020-01-20 22:36:38');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pedidos`
--

CREATE TABLE `pedidos` (
  `id` int(11) NOT NULL,
  `usuario` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `total` double(6,2) NOT NULL,
  `status` tinyint(4) NOT NULL,
  `fecha` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `pedidos`
--

INSERT INTO `pedidos` (`id`, `usuario`, `total`, `status`, `fecha`) VALUES
(35, '2281895262', 2832.00, 1, '2020-01-20 17:37:33'),
(34, '2281895262', 192.00, 1, '2020-01-20 16:48:51'),
(33, '2281042014', 2085.40, 1, '2020-01-20 16:48:22'),
(32, '2281895262', 597.40, 1, '2020-01-20 10:02:39'),
(31, '2281895262', 597.40, 1, '2020-01-20 10:00:56'),
(30, '2281895262', 1129.20, 1, '2020-01-20 09:58:13'),
(36, 'rafa', 931.80, 1, '2020-01-20 19:22:40'),
(37, 'rafa', 197.40, 1, '2020-01-20 19:29:30'),
(39, 'rafa', 3024.40, 1, '2020-01-20 22:35:39');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuarios`
--

CREATE TABLE `usuarios` (
  `id_cliente` int(11) NOT NULL,
  `nombre` varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL,
  `telefono` bigint(20) DEFAULT NULL,
  `email` varchar(300) COLLATE utf8_unicode_ci DEFAULT NULL,
  `calle_numero` varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL,
  `colonia` varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL,
  `usuario` varchar(300) COLLATE utf8_unicode_ci DEFAULT NULL,
  `password` varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `usuarios`
--

INSERT INTO `usuarios` (`id_cliente`, `nombre`, `telefono`, `email`, `calle_numero`, `colonia`, `usuario`, `password`) VALUES
(1, 'Jhonatan chÃ¡vez', 8520, 'jhonatanchavezcastillo@gmail.com', 'Octavio Blancas', 'Fraccionamiento Lucas martÃ­n', '2281895262', '7110eda4d09e062aa5e4a390b0a572ac0d2c0220'),
(2, 'Rafael Hdez', 2281102030, 'rafael@gmail.com', 'Allende 51', 'Centro', 'rafa', '7110eda4d09e062aa5e4a390b0a572ac0d2c0220'),
(5, 'AndrÃ©s mancera barradas', 2281042014, 'morvidos@hotmail.com', 'Ignacio Aldama', 'centro', '2281042014', 'e411a490148911bb6ee16beaa6f794a437b3bc9d');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `detalle_pedidos`
--
ALTER TABLE `detalle_pedidos`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `pedidos`
--
ALTER TABLE `pedidos`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  ADD PRIMARY KEY (`id_cliente`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `detalle_pedidos`
--
ALTER TABLE `detalle_pedidos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- AUTO_INCREMENT de la tabla `pedidos`
--
ALTER TABLE `pedidos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=40;

--
-- AUTO_INCREMENT de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  MODIFY `id_cliente` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
