var select_forma_pago = 0;
var forma_pago = 0;
/*Aumentar la cantidad del artículo*/
$(document).on("click", "#article-mas", function(){
    let container_input = $(this).parents('#container-cantidad');
        container_input = container_input.find('input#input-cantidad').val();
    let container_input_hidden = $(this).parents('#container-cantidad');
    container_input_hidden = container_input_hidden.find('input#valor-app').val();
    // alert(container_input_hidden)
    let cantidad = Number(container_input) + Number(container_input_hidden);
    let existencia = $(this).parents('#li-article').find('#existencias').data('existencias');
    if(cantidad > existencia){ alert("Sólo hay " + existencia + " en existencias"); return false; }
    /*Sumar totales*/
    let precio = $(this).parents('#li-article').find('#price-article').data('price');
    $(this).parents('#container-cantidad').find('input#input-cantidad').val(cantidad);
    $(this).parents('#li-article').find("#total-price-article").text("Total $" + (cantidad * precio));
    //Actualizar cantidad del producto en carrito de compra
    let li = $(this).parents('#li-article');
    actualiza_cantidad_mas(li.find("#title-article").text());
});
/*Disminuir la cantidad del artículo*/
$(document).on("click", "#article-menos", function(){
    let container_input = $(this).parents('#container-cantidad');
        container_input = container_input.find('input#input-cantidad').val();
    let container_input_hidden = $(this).parents('#container-cantidad');
    container_input_hidden = container_input_hidden.find('input#valor-app').val();
    // alert(container_input_hidden)
    if(container_input > container_input_hidden){
        let cantidad = Number(container_input) - Number(container_input_hidden);
        $(this).parents('#container-cantidad').find('input#input-cantidad').val(cantidad);
        /*Restar total*/
        let precio = $(this).parents('#li-article').find('#price-article').data('price');
        $(this).parents('#container-cantidad').find('input#input-cantidad').val(cantidad);
        $(this).parents('#li-article').find("#total-price-article").text("Total $" + (cantidad * precio));
        //Actualizar cantidad del producto en carrito de compra
        let li = $(this).parents('#li-article');
        actualiza_cantidad_menos(li.find("#title-article").text());
    }
});
/*Agregar al carrito de compras*/
var count_item_car = 0;
$(document).on("click", "#add-article", function(){
    $(this).addClass('hidden');//ocultar
    let input_group = $(this).parents('.input-group-btn');
    let item = $(this).parents('#li-article');
    if(count_item_car >= 0){
        count_item_car = count_item_car + 1;
        $("#count-item-car").text(count_item_car);
        input_group.find("#remove-article").removeClass("hidden");
        add_item(item);
    }
});
/*Quitar al carrito de compras*/
$(document).on("click", "#remove-article", function(){
    $(this).addClass('hidden');//ocultar
    let input_group = $(this).parents('.input-group-btn');
    let li = $(this).parents('#li-article');
    if(count_item_car >= 0){
        count_item_car = count_item_car - 1;
        $("#count-item-car").text(count_item_car);
        input_group.find("#add-article").removeClass("hidden");
        remove_article(li.find("#title-article").text());//remove_article
    }
});

/*Quitar al carrito de compras desde DETALLE DE COMPRA*/
$(document).on("click", ".quitar-produto-detalle", function(){
    let li = $(this).parents('#li-article');
    let id = $(this).data('id');
    $('#remove-article.' + id).click();
    li.remove();
});

/*Remover producto del carrito de compras*/
function remove_article(item){
    $.each( articulos_carrito, function( key, value ) {
        if(value.nombre.replace(/ /g, "") == item.replace(/ /g, "")){ value.status = 0; }
    });
}
/*Actualizar cantidad del producto del carrito*/
function actualiza_cantidad_menos(item){
    $.each( articulos_carrito, function( key, value ) {
        if(value.nombre.replace(/ /g, "") == item.replace(/ /g, "")){ value.cantidad = parseInt(value.cantidad) - 1; }
    });
}
/*Actualizar cantidad del producto del carrito*/
function actualiza_cantidad_mas(item){
    $.each( articulos_carrito, function( key, value ) {
        if(value.nombre.replace(/ /g, "") == item.replace(/ /g, "")){ value.cantidad = parseInt(value.cantidad) + 1; }
    });
}
/*Agregar item al carrito de compra*/
var articulos_carrito = [];
var articulos_carrito_tem = [];
function add_item(item){
    let id = item.find('#add-article').data('id');
    let img = item.find('img').attr('src');
    let nombre = item.find('#title-article').text() + " " + item.find('#description-article').text();
    let precio_normal = item.find('#price-article').data('price');
    let precio_descuento = item.find('#precio-descuento').data('descuento');
    let total = parseFloat(precio_normal) * parseFloat(item.find('#input-cantidad').val());
    let total_descuento = parseFloat(precio_descuento) * parseFloat(item.find('#input-cantidad').val());
    let total_tem = precio_descuento == undefined? total: total_descuento;
    let precio_total = total_tem;
    let cantidad = item.find('#input-cantidad').val();
    /*Agregar artículos al carrito*/
    articulos_carrito.push({ 
        id: id,
        img: img,
        nombre: nombre,
        precio_normal: precio_normal,
        precio_descuento: precio_descuento,
        precio_total: precio_total,
        cantidad: cantidad,
        status: 1
    });
}

/*Iniciar sesión*/
function iniciar_sesion(){
    let data = {
        usuario: $('#datos-sesionModal #nombre-sesion').val(),
        password: $('#password-sesion').val(),
    }
    $("#loadingModal").modal({ backdrop: 'static', keyboard: false, show: true });
    $.ajax({
        type:"POST",
        url:"servicios/CRUD/login.php",
        data:data,
        dataType:"json",
        success:function(response){
            if(response.success){
                sessionStorage.setItem('usuario', data.usuario);
                obtener_datos_entrega();
            }else{
                alert("No pudo iniciar sesión, usuario y contraseña pueden ser incorrectos.");
            }
            $("#loadingModal").modal("hide");
        }
    });
}
/*Obtener información de entrega*/
function obtener_datos_entrega(){
    $.ajax({
        type:"POST",
        url:"servicios/CRUD/datos_entrega.php",
        data:{user: sessionStorage.getItem("usuario")},
        dataType:"json",
        success:function(response){
            $("#loadingModal").modal("hide");
            $("#datos-sesionModal").modal("hide");
            $("#datos-entregaModal").modal({ backdrop: 'static', keyboard: false, show: true });
            llenar_datosEntrega(response.datos_entrega[0]);
        }
    });
}
/*Llenar la información del formulario*/
function llenar_datosEntrega(data){
    $('#datos-entregaModal #nombre').val(data.nombre);
    $('#datos-entregaModal #telefono').val(data.telefono);
    $('#datos-entregaModal #correo').val(data.email);
    $('#datos-entregaModal #calle').val(data.calle);
    $('#datos-entregaModal #colonia').val(data.colonia);
}
/*Enviar datos del pedido*/
function enviar_pedido(){
    let data = {
        user: sessionStorage.getItem("usuario"),
        articulos: articulos_carrito,
        total_pedido: total_pedido,
        forma_pago: forma_pago,
        lista_producto: $('#detalle-compraModal #lista-mandado').val(),
        presupuesto_mandado: $('#detalle-compraModal #presupuesto-mandado').val(),
    }
    $.ajax({
        type:"POST",
        url:"servicios/CRUD/guardarPedido.php",
        data: data,
        dataType:"json",
        success:function(response){

        }
    });
}

function guardar_datos_envio(){

    let nombre = $('#datos-entregaModal #nombre').val();
    let telefono = $('#datos-entregaModal #telefono').val();
    let correo = $('#datos-entregaModal #correo').val();
    let calle = $('#datos-entregaModal #calle').val();
    let colonia = $('#datos-entregaModal #colonia').val();
    let data = {
            user: sessionStorage.getItem('usuario'),
            nombre: nombre,
            telefono: telefono,
            correo: correo,
            calle: calle,
            colonia: colonia
        }
    $("#datos-entregaModal").modal("hide");
    $("#loadingModal").modal({ backdrop: 'static', keyboard: false, show: true });
    $.ajax({
        type:"POST",
        url:"servicios/CRUD/guardar_datos_envio.php",
        data:data,
        dataType:"json",
        success:function(response){
            $("#loadingModal").modal('hide');
            if(response.success){
                $("#datos-entregaModal").modal("hide");
                $("#formaPago-Modal").modal({ backdrop: 'static', keyboard: false, show: true });
            }else{
                $("#datos-entregaModal").modal({ backdrop: 'static', keyboard: false, show: true });
            }
        }
    })
}

