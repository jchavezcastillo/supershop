/*Aumentar la cantidad del artículo*/
$(document).on("click", "#article-mas-oferta", function(){
    let container_input = $(this).parents('#container-cantidad');
    container_input = container_input.find('input#input-cantidad').val();
    let cantidad = parseInt(container_input) + 1;
    let existencia = $(this).parents('#li-article').find('#existencias').data('existencias');
    if(cantidad > existencia){ alert("Sólo hay " + existencia + " en existencias"); return false; }
    /*Sumar totales*/
    let precio = $(this).parents('#td-article-oferta').find('#total-price-article').data('price');
    $(this).parents('#container-cantidad').find('input#input-cantidad').val(cantidad);
    $(this).parents('#td-article-oferta').find("#total-price-article").text("Total $" + (cantidad * precio));
});
/*Disminuir la cantidad del artículo*/
$(document).on("click", "#article-menos-oferta", function(){
    let container_input = $(this).parents('#container-cantidad');
    container_input = container_input.find('input#input-cantidad').val();
    if(container_input > 1){
        let cantidad = parseInt(container_input) - 1;
        $(this).parents('#container-cantidad').find('input#input-cantidad').val(cantidad);
        /*Restar total*/
        let precio = $(this).parents('#td-article-oferta').find('#total-price-article').data('price');
        $(this).parents('#container-cantidad').find('input#input-cantidad').val(cantidad);
        $(this).parents('#td-article-oferta').find("#total-price-article").text("Total $" + (cantidad * precio));
    }
});