 /*Abrir modal DETALLE DE LA COMPRA*/
 var total_pedido = 0;
$(document).on("click", "#button-comprar", function(){
    $("#detalle-compraModal").modal({ backdrop: 'static', keyboard: false, show: true });
    /*Mostrar listado de productos*/
    let li = '';
    let total = 0;
    $.each( articulos_carrito, function( key, value ) {
        if(value.status == 1){
            let precio = value.precio_descuento == undefined? value.precio_normal: value.precio_descuento;
            li += '<li id="li-article" class="list-group-item">\
                <div class="media"> \
                    <div class="media-left"> \
                        <a href="#"> \
                            <img id="img-article" alt="112x150" class="media-object" style="width: 112px;" src="'+ value.img +'" data-holder-rendered="true"> \
                        </a> \
                    </div> \
                    <div class="media-body"> \
                        <div>\
                            <h4 class="media-heading">\
                                <span id="title-article" style="font-weight: bold;">'+ value.nombre +'</span> \
                            </h4> \
                            <div class="color-blue">Cantidad: '+ value.cantidad +'</div> \
                            <div class="color-blue">Precio: $'+ precio +'</div> \
                            <div class="color-blue">Total: $'+ value.precio_total +'</div> \
                        </div>\
                    </div>\
                    <div class="media-left"> \
                        <div class="quitar-produto-detalle" data-id="' + value.id + '"><span class="glyphicon glyphicon-trash"></span></div>\
                    </div> \
                </div>\
            </li>';
            total = total + value.precio_total;
        }
    });
    $('#detalle-compraModal ul').html(li);
    $('#detalle-compraModal #total-compra').html('TOTAL: $' + total);
    $('#detalle-compraModal #total-compra').attr('totalcompra', total);
    total_pedido = total;
});
/*Regresar*/
$(document).on("click", "#close-modal", function(){
    $("#detalle-CompraModal").modal("hide");
});
/*Abrir modal DETALLE DE LA ENTREGA*/
$(document).on("click", "#button-datos-entregaModal", function(){
    $("#detalle-compraModal").modal("hide");
    $("#datos-entregaModal").modal({ backdrop: 'static', keyboard: false, show: true });
});
/*Regresar*/
$(document).on("click", "#button-detalle-compraModal", function(){
    $("#datos-sesionModal").modal("hide");
    $("#detalle-compraModal").modal({ backdrop: 'static', keyboard: false, show: true });
});
/*Regresar a detalle pedido*/
$(document).on("click", "#button-detalle-compraModal-volver", function(){
    $("#datos-entregaModal").modal("hide");
    $("#detalle-compraModal").modal({ backdrop: 'static', keyboard: false, show: true });
});
/*Abrir modal FORMA DE PAGO*/
$(document).on("click", "#button-formaPago-Modal", function(){
    /*Guardar datos de envio*/
    guardar_datos_envio();
});
/*Regresar*/
$(document).on("click", "#button-datos-entregaModal-regresar", function(){
    $("#formaPago-Modal").modal("hide");
    $("#datos-entregaModal").modal({ backdrop: 'static', keyboard: false, show: true });
});
/*Abrir modal CONFIRMACIÓN DE PAGO*/
/*$(document).on("click", "#button-confirmacion-compraModal", function(){
    $("#formaPago-Modal").modal("hide");
    $("#confirmacion-compraModal").modal({ backdrop: 'static', keyboard: false, show: true });
});*/
/*Regresar*/
$(document).on("click", "#close-modal-confirmacion", function(){
    $("#confirmacion-compraModal").modal("hide");
});

/*Mostrar ventana de inicio se sesión*/
$(document).on("click", "#button-sesionModal", function(){
    if(sessionStorage.getItem("usuario") == null){
        $("#detalle-compraModal").modal("hide");
        $("#datos-sesionModal").modal({ backdrop: 'static', keyboard: false, show: true });
    }else{
        obtener_datos_entrega();
    }
});
/*Mostrar loading*/
$(document).on("click", "#button-inicio-sesion", function(){
    iniciar_sesion();
});
/*Mostrar enviar pedido*/
$(document).on("click", "#button-confirmacion-compraModal", function(){
    /*Enviar datos del pedido*/
     if(select_forma_pago == 0){ 
        alert("Elige una forma de pago"); 
    }else{ 
        enviar_pedido();
        $("#confirmacion-compraModal").modal({ backdrop: 'static', keyboard: false, show: true });
    }
});
/*Regresar*/
$(document).on("click", "#close-modal-forirmacion-pago", function(){
    window.location.reload();
});
/*Registrate*/
$(document).on("click", "#button-registrarModal", function(){
    $("#datos-sesionModal").modal("hide");
    $("#registerModal").modal({ backdrop: 'static', keyboard: false, show: true });
});

/*Regresar del registro*/
$(document).on("click", "#button-volver-registro", function(){
    $("#datos-sesionModal").modal({ backdrop: 'static', keyboard: false, show: true });
    $("#registerModal").modal("hide");
});